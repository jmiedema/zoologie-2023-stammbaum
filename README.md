# Zoologie 2023 Stammbaum



## Disclaimer
![tree](/Untitled_Diagram.drawio.png)

Dieser Stammbaum ist im Rahmen des Kurses "Zoologische Grundübungen" erstellt worden. Ich erhebe keinen Anspruch auf Vollständigkeit/ Korrektheit. Der Staummbaum repräsentiert keinen phylogenetischen Baum dahingehend, dass die Astlängen keine richtig Distanzen repräsentieren: Der Stammbaum ist vielmehr eine Zusammenstellung vieler einzelner, es basiert also auf keiner Distanzmatrix, er soll einen Überblick über die phylogenetischen Verhältnisse geben. 